let closetedMen;
let closetedWomen;
let openlyMen;
let openlyWomen;
let newsPic;
let carpet;
let frame2;
let frame;
let women;
let men;
let state = true;
let sad = false;

function preload() {
  closetedWomen = loadImage("close_women.png");
  closetedMen = loadImage("closed_men.png");
  openlyWomen = loadImage("open_women.png");
  openlyMen = loadImage("open_men.png");
  fullConsequences = loadImage("consequences.jpg");
  carpet = loadImage("carpet.jpg");
  frame2 = loadImage("frame2.jpg");
  frame = loadImage("frame.jpg");
}

function setup() {
  createCanvas(1500, 750);
  women = new Women(600, 359);
  men = new Men(900, 359);
  background("white");

  yes = createButton("Yes");
  yes.position(1500/2-100, 750/2+130);
  yes.style('fontSize','25px');
  yes.mousePressed(yeah);
  no = createButton("No");
  no.position(1500/2+50, 750/2+130);
  no.style('fontSize','25px');
  no.mousePressed(nope)
}

function draw() {
  if (state === true){
  roomDisplay();
  doorsDisplay();
  hoverMouse();
  yes.hide();
  no.hide();
} else {
  yes.show();
  no.show();
  roomDisplay();
  areYouSure();
}

if (sad === true){
  yes.hide();
  no.hide();
  consequences();
  }
}

function roomDisplay() {
  stroke("black");
  strokeWeight(4);
  line(1500/4, 750-270, 1500-(1500/4), 750-270);
  line(1500/4, 750-270, 0, 750);
  line(1500-(1500/4), 750-270, 1500, 750);
  line(1500-(1500/4), 750-270, 1500-(1500/4), 0);
  line(1500/4, 750-270, 1500/4, 0);
}

function doorsDisplay() {
  women.displays();
  men.displays();
  furniture();
}

function hoverMouse() {
  //women
  if (mouseX < 722 && mouseX > 572 && mouseY > 250 && mouseY < 495) {
    women.open();
  } else if (mouseX > 722 && mouseX < 572 && mouseY < 250 && mouseY > 495) {
    women.delete();
  }

  // men
  if (mouseX > 820 && mouseX < 970 && mouseY > 250 && mouseY < 495) {
    men.open();
  } else if (mouseX < 820 && mouseX > 970 && mouseY < 250 && mouseY > 495) {
    men.delete();
  }
}

function mouseClicked() {
  if (mouseX < 722 && mouseX > 572 && mouseY > 250 && mouseY < 495) {
    //women
    state = false;
  }

  if (mouseX > 820 && mouseX < 970 && mouseY > 250 && mouseY < 495) {
    //men
    state = false;
  }
}

function areYouSure() {
  fill("white");
  rectMode(CENTER);
  rect(1500/2, 750/2, 600, 400);
  stroke("black");
  noStroke();
  fill("black");
  textSize(40);
  textAlign(CENTER);
  text("Are you sure?", 1500/2, 750/2-50);
}

function yeah() {
  sad = true;
}

function nope() {
  background("white");
  roomDisplay();
  state = true;
}

function consequences(){
  image(fullConsequences, 750, 350, 1500, 800);
}

function furniture(){
  image(frame, 100, 350, 300, 400);
  image(carpet, 750, 700, 900, 300);
  image(frame2, 1400, 350, 300, 400);
}
