[See my project](https://emmathing.gitlab.io/aesthetic-programming-2.0/MiniX1/)

[See my repository](https://gitlab.com/EmmaThing/aesthetic-programming-2.0/-/blob/master/MiniX1/sketch.js)

This is a screenshot of what I made:
![](MiniX1-hjerte.png)

In my first MiniX, I have created what I would call an asymmetric heart, out of various shapes and colors. I choose to play around with these two things, to get a better understanding of what the different variations of numbers and signs mean, when you write a certain code. I created the aesthetic of a heart to underline the idea, that the choices you make, while designing and coding, are not neutral. I used a lot of shapes, that can be understood as neutral, but by putting them together as a heart, it suddenly creates something that can be understood completely different. For example, as a connotation for love. 

I used a lot of different syntaxes, to challenge myself in this assignment. In the beginning I used the code called colorMode(HSB), but it gave me trouble, when I wanted to create the yellow color with the code (255, 204, 0), so I added the code: colorMode(RGB), which then gave me the desired color. To create a thick blue line around all of the shapes, I used a code called: stroke(color(0, 0, 255)) and to make the rest of the shapes that I did not give a specific color, I used: fill(random(600), 600, 600). This code made them flash in different kinds of light blue. I learned many things from making this sketch, because it was my first time, so everything was new.

I believe that there are many similarities between writing and coding. They both allow you to express yourself and as I mentioned earlier coding is not neutral. It has power just as the written word. Therefor I also find it very important to understand, make and reflect on coding. It is used today in so many ways and when we are informed properly about it, it makes it possible for us to have constructive discussions about technology and what it means for society.
