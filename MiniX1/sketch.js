function setup() {
  createCanvas(800, 700);
  print("hello world");
  colorMode(HSB);
  colorMode(RGB);
}
function draw() {
  background('#222222');
  let g = color('#fae');
  fill(g);
  ellipse(650, 400, 60, 90);
  fill(0, 0, 255);
  square(425, 220, 55);
  fill('red');
  rect(150, 355, 50, 80);
  fill('rgba(0,255,0, 0.25)')
  circle(620, 460, 50);
  let b = color(300, 90, 110);
  fill(b);
  square(310, 550, 52, 20);
  let e = color(400, 100, 60);
  fill(e);
  triangle(435, 580, 575, 540, 600, 480);
  let c = color ('#0f0');
  fill(c);
  ellipse(265, 525, 40, 70);
  fill(300, 100, 700);
  arc(290, 210, 80, 80, 0, PI+QUARTER_PI, PIE);

  stroke(color(0, 0, 255));
  strokeWeight(8);
  let d = color (255, 204, 0);
  fill(d);
  triangle(530, 190, 570, 240, 485, 240);

  fill(random(600), 600, 600);
  square(320, 220, 60, 25, 20, 15, 10);
  circle(400, 300, 70);
  square(575, 220, 55, 20);
  quad(710, 280, 660, 240, 635, 260, 650, 350);
  arc(220, 470, 80, 80, -50, PI+QUARTER_PI, CHORD);
  arc(400, 600, 80, 80, 150, PI+QUARTER_PI, PIE);
  triangle(160, 220, 165, 355, 250, 220);
  circle(300, 550, 30);
}
