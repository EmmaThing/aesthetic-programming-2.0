function setup() {
  createCanvas(windowWidth, windowHeight);
  colorMode(HSB);
}

function draw() {
  background (51);
  stroke(80);
  strokeWeight(4);
  noFill();

  if (mouseX > 90 && mouseX < 310 && mouseY > 190 && mouseY < 400){
    fill(random(245), 245, 245, 245);
  }

  //face 1
  ellipse(200, 290, 220, 200);

  //background for face 2
  noStroke();
  rect(400, 150, 280, 280);

  //face 2
  stroke(51);
  strokeWeight(4);
  ellipse(540, 290, 140, 250)

  //chin
  stroke(65);
  strokeWeight(4);
  arc(200, 330, 100, 98, QUARTER_PI,PI-QUARTER_PI);

  //cheeks
  stroke(65);
  strokeWeight(4);
  ellipse(265, 310, 80, 66);
  ellipse(135, 310, 80, 66);

  //color in cheeks
  fill(330, 100, 100);
  noStroke();
  ellipse(245, 300, 40, 33);
  ellipse(155, 300, 40, 33);

  //eyes
  fill(100);
  stroke(204,102,0);
  ellipse(225,260,30,36);
  ellipse(175,260,30,36);
  fill(0);
  ellipse(175, 266, 20, 20);
  ellipse(225, 266, 20, 20);

  //eyes 2
  if (mouseX > 90 && mouseX < 310 && mouseY > 190 && mouseY < 400){
  fill(100);
  stroke(204,102,0);
  ellipse(560,260,30,36);
  ellipse(520,260,30,36);
  fill(0);
  ellipse(560, 266, 20, 20);
  ellipse(520, 266, 20, 20);
  }

  //mouth
  noFill();
  arc(200, 310, 57, 56, QUARTER_PI,PI-QUARTER_PI);

  //mouth 2
  if (mouseX > 90 && mouseX < 310 && mouseY > 190 && mouseY < 400){
  noFill();
  arc(540, 350, 57, 56, QUARTER_PI,PI-QUARTER_PI);
  }

  //cheeks 2
  noFill();
  stroke(51);
  strokeWeight(4);
  arc(610, 340, 80, 63,PI,3*PI/2);
  noFill();
  arc(465, 338, 86, 65, PI-4.5, 3/PI*6.6);

  //color in cheeks 2
  if (mouseX > 90 && mouseX < 310 && mouseY > 190 && mouseY < 400){
  fill(330, 100, 100);
  noStroke();
  ellipse(580, 302, 40, 33);
  ellipse(500, 302, 40, 33);
  }

  //nose 1
  fill(80)
  ellipse(200, 290, 10, 10)

  //nose 2
  fill(51)
  ellipse(540, 290, 10, 10)


// I couldn't make the following work to make a polygon:
//  translate(width * 0.8, height * 0.5);
//    polygon(0, 0, 70, 7);

//function polygon(x, y, radius, npoints) {
  //  let angle = TWO_PI / npoints;
  //  beginShape();
  //  for (let a = 0; a < TWO_PI; a += angle) {
  //    let sx = x + cos(a) * radius;
    //  let sy = y + sin(a) * radius;
  //    vertex(sx, sy);
  //  }
  //  endShape(CLOSE);
}
