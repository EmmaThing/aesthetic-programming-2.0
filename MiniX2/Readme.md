### MiniX2

[See my project](https://emmathing.gitlab.io/aesthetic-programming-2.0/MiniX2/)

[See my repository](https://gitlab.com/EmmaThing/aesthetic-programming-2.0/-/blob/master/MiniX2/sketch.js)

Screenshots of my design:

![](MiniX2.1-emoji.png)
![](MiniX2-emoji.png)

This MiniX-assignment was about making two emojis using various shapes and colors. At first, I wanted to make an emoji that looked like a face, but as far from _“the shape of a human face”_ as possible. I liked this idea, because I thought it would make it harder, for people to compare themselves with it, though I still wanted people to be able to use it as an expression for emotions. I decided that the outline of the shapes representing the emojis, should be a polygon and a triangle. To make a polygon in p5.js, I found out that I needed to make my own function. On p5.js’ own website, I found a guide to make the function I needed, but I couldn’t make it work (the code is in end of my repository). In my frustration I went to look at the emojis in my phone. I discovered that there was no emojis representing humans that are either thick or thin, which gave me the idea, I decided to go through with. With different functions and sizes of ellipses and arcs, I created _the thick emoji_. The thick emoji is the first one you see on the screen (the one to the left). It has pink cheeks, eyes, a nose, an arc in black pointing upwards as a smile and lastly two ellipses and an arc, in a more shuttle gray color than the background, to make the illusion of big cheeks and a doublechin. I used syntaxes as stroke(), strokeWeight() and fill() for the colors that are visible at first. Other colors become visible, when interacting with the emoji. When you hold the mouse over the emoji, it starts flashing in different random colors and the second emoji appears in a square to the right. This is the emoji I call _the thin emoji_. I made the square by using the syntax rect(400, 150, 280, 280). The syntaxes of the second emoji are similar to the first, but the strokes are now the same color as the background, and it has arcs, instead of ellipses as cheeks, to make the illusion of cheekbones. 
I used the Boolean method with if-statements to make the effect with the mouse, but I could only make it work in a square, so because the face is an ellipse it works in the corners outside the ellipse too, which wasn’t my intention. 
I like the syntax; createCanvas(windowWidth, windowHeight), that makes the size of the canvas the same size as your window, because it fills it all out, but in order for you to see both the thick emoji and the thin emoji fully, the size of the window needs to be at least 775 x 525.

I had a lot of emotions while creating this assignment. I found it both fun, frustrating, interesting and confusing, all in all very challenging. Coding the emojis was almost the easiest part, coming up with the idea and thinking about its context was the hardest. I didn’t find it hard because I didn’t have thoughts about, but because I think, I had way too many thoughts and feelings about it. In the context of today, especially in design, the idea about including and excluding people is often discussed. I find it very hard to navigate in this discussion. Sometimes it makes me think that you can’t do anything right, because it is impossible to include everyone, which makes me wonder if it is even better to exclude everyone? 

In the video _”Modifying the Universal - Femke Snelting”_, Femke Snelting speaks about the skin tone modifier, that was added to some of the emojis in 2015. It is a function, that makes it possible for people to choose between five different skin tones to give the emoji, aswell as a _”neutral”_ one, in the usual yellow color. This was the inspiration about the random coloring of my emojis, when you place the mouse on _the thick emoji_. When you haven’t used an emoji with the possibility of the skin tone modifier before, you actively need to make a decision of which tone you want. I found that kind of provoking, because I don’t believe that you should have to choose “side”. My emojis have every color and it is not your own choice, which I find more neutral. Including and excluding has many aspects and it depends on the person who sees it. 

I like my design, especially because of the random coloring and because I find it both, including, excluding, ironic and provoking to make a “thick” and a “thin” emoji, as well as calling them thick and thin. I think it could be seen as harsh or as a bad idea to implement emojis like mine, since I think they could be used in lots of “wrong” ways, for example for bullying, as the weight of a person can be a impactful factor for that. On the other hand, I believe that it could be a good thing, to implement my emojis to normalize that we are all different sizes and shapes, and that there is no right way to be, which today with body positivity campagnas could be positive impact.

This MiniX is my second attempt to code. I already feel like I have learned a lot, even though a lot of things didn't go as I wanted. This time it was easier to start coding, because I had gained basic knowledge.

References:

[Modifying the Universal—Femke Snelting](https://www.youtube.com/watch?v=ZP2bQ_4Q7DY)

[3.4: Boolean Variables - p5.js Tutorial](https://www.youtube.com/watch?v=Rk-_syQluvc)

[Polygon example](https://p5js.org/examples/form-regular-polygon.html)

[references on p5.js.com](https://p5js.org/reference/)
