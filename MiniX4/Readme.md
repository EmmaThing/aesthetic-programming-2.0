# We got your data!

[See my project](https://emmathing.gitlab.io/aesthetic-programming-2.0/MiniX4/)

[See my repository](https://gitlab.com/EmmaThing/aesthetic-programming-2.0/-/blob/master/MiniX4/sketch.js)

### Screenshots of my design:

![](start.png)
![](red.png)
![](false.png)
![](true.png)


## MiniX4 

In our society today, the digital technologies are a big part of the everyday life, but it is not without its consequences. According to Shoshana Zuboff the ignorance of the user is especially an issue. In the documentary _Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary_, she says _“our ignorance is their bliss"_ (12.19), which speaks exactly upon this topic and about the danger of surveillance capitalism. Shoshana Zuboff means that we need to be more critical towards the use of our data. One major thing we need to do, is demand transparency from the companies taking part in surveillance capitalism, in order to get important information about what happens to the data we provide, when using digital technologies.
With my design I want to contribute to this demand. Therefore, I created a design that can be seen as a critic of the way companies in surveillance capitalism takes and sells the users data, without letting them know to who or why. My design shows four squares in four different colors (red, green, yellow and blue). Each square has a button that says “choose …” and then the specific color. Besides the buttons and the squares, there is a text on the first display that encourages people to pick a color, when a color is picked, the screen turns into the chosen color and a text with a response to the color, appears on the display, as well as two buttons, that let the user agree og disagree with the statement. When the user interacts with these buttons a black screen appears and a text that informs them that their data has been _”sold”_. It says nothing about to whom or what it is going to be used for. 
Making this design I used a few syntaxes, that I haven’t used before. I explored the use of making my own functions and what I was able to do with them. I found making my own function a quite liberating experience, because I felt that it gave me more control and made it easier to work with the different stages of my design. I also used the syntaxes to create buttons for the first time, which I really liked, since I think it is fun to make a design that is more interactive.

The theme _capture all_ fits this design very well. It captures the data of the mouse clicks on the buttons and the choices made by the user. If the design was implemented, it could contribute to what is called _behavioral surplus_. The behavioral surplus is a sort of profile made on the user used to influence or predict their coming acts. This way to capture data can affect the use of digital technologies both good and bad. The information can be abused if it is sold to _’the wrong people’_. When people interact with my design and pronounce what they may feel, it could be used to give them an ad to provoke a certain action. If you feel sad, you could for example be presented with something, that is thought to make people happier and then you will click on the ad, which contributes to what the companies want. In situations like these companies are able to manipulate the users and because lack of transparency, they can make them do something that they don’t want to or that they don’t know they are doing, because they aren’t informed. This could mean huge cultural implications.

### References:
Shoshana Zuboff. _Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary_. (2019)
[link](https://youtu.be/hIXhnWUmMvw.)
