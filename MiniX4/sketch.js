//I needed to create diffrent button-names, because the web editor couldn't figure out what to do, when I in the beginning, made only one button name 
  let button;
  let button2;
  let button3;
  let button4;
  let buttontrue;
  let buttonfalse;

function setup() {
  createCanvas(windowWidth, windowHeight);


// I needed to put the buttons, the rectangles and the text in setup so it would only display one time
// this made it easier to controle them 
// the button for the blue rectangle
    button = createButton('Choose blue');
    button.position(windowWidth/1.7, windowHeight/2);

//calls the "setcolblue"-function, when the mouse is pressed on the "choose blue"-button
    button.mousePressed(setcolblue);

    //buttonMode(CENTER); - A code that didn't work. I wanted to center the buttons coordinates 

// the button for the green rectangle
    button2 = createButton('Choose green');
    button2.position(windowWidth/3.2, windowHeight/2);
    button2.mousePressed(setcolgreen);

// the button for the red rectangle
    button3 = createButton('Choose red');
    button3.position(windowWidth/3.1, windowHeight/6);
    button3.mousePressed(setcolred);

// the button for the yellow rectangle
    button4 = createButton('Choose yellow');
    button4.position(windowWidth/1.7, windowHeight/6);
    button4.mousePressed(setcolyellow);

    fill('blue');
    rectMode(CENTER);
    rect(windowWidth/1.6, windowHeight/2, 200, 200);


    fill('green');
    rectMode(CENTER);
    rect(windowWidth/2.9, windowHeight/2, 200, 200);


    fill('red');
    rectMode(CENTER);
    rect(windowWidth/2.9, windowHeight/6, 200, 200);


    fill('yellow');
    rectMode(CENTER);
    rect(windowWidth/1.6, windowHeight/6, 200, 200);

//calling the function
    showtext();
}

function draw() {
}


function showtext(){
  fill('black');
  textSize(40);
  textAlign(CENTER);
  text('Choose a color and found out what you feel today (; :)', windowWidth/2, windowHeight/1.3);
}

// when the red button is pressed, the following will happen. Clears interface and display text, aswell as the true and false button
  function setcolred(){
    clear();
    button.hide();
    button2.hide();
    button3.hide();
    button4.hide();
    background(255, 0, 0);
    textAlign(CENTER);
    text('Your data tells us that you are angry today', windowWidth/2, windowHeight/2);

    buttontrue = createButton('It is true');
    buttontrue.position(windowWidth/2.4, windowHeight/1.6);
// calls the "great"-function, when the mouse is pressed on the "It is true"-button
    buttontrue.mousePressed(great);

    buttonfalse = createButton('It is false');
    buttonfalse.position(windowWidth/2, windowHeight/1.6);
// calls the "tolate"-function, when the mouse is pressed on the "It is false"-button
    buttonfalse.mousePressed(tolate);
  }

//when the blue button is pressed, the following will happen (the same as the red)
  function setcolblue(){
    clear();
    button.hide();
    button2.hide();
    button3.hide();
    button4.hide();
    background('blue');
    textAlign(CENTER);
    text('Your data tells us that you are sad today', windowWidth/2, windowHeight/2);

    buttontrue = createButton('It is true');
    buttontrue.position(windowWidth/2.4, windowHeight/1.6);
    buttontrue.mousePressed(great);

    buttonfalse = createButton('It is false');
    buttonfalse.position(windowWidth/2, windowHeight/1.6);
    buttonfalse.mousePressed(tolate);
  }

//when the yellow button is pressed, the following will happen (the same as the red)
  function setcolyellow(){
    clear();
    button.hide();
    button2.hide();
    button3.hide();
    button4.hide();
    background('yellow');
    textAlign(CENTER);
    text('Your data tells us that you are happy today', windowWidth/2, windowHeight/2);

    buttontrue = createButton('It is true');
    buttontrue.position(windowWidth/2.4, windowHeight/1.6);
    buttontrue.mousePressed(great);

    buttonfalse = createButton('It is false');
    buttonfalse.position(windowWidth/2, windowHeight/1.6);
    buttonfalse.mousePressed(tolate);
  }

//when the green button is pressed, the following will happen (the same as the red)
  function setcolgreen(){
    clear();
    button.hide();
    button2.hide();
    button3.hide();
    button4.hide();
    background('green');
    textAlign(CENTER);
    text('Your data tells us that you are jealous today', windowWidth/2, windowHeight/2);

    buttontrue = createButton('It is true');
    buttontrue.position(windowWidth/2.4, windowHeight/1.6);
    buttontrue.mousePressed(great);

    buttonfalse = createButton('It is false');
    buttonfalse.position(windowWidth/2, windowHeight/1.6);
    buttonfalse.mousePressed(tolate);
  }

//this is the function for the "It is true" button
  function great(){
    clear();
    button.hide();
    button2.hide();
    button3.hide();
    button4.hide();
    background('black');
    fill('white');
    textSize(30);
    textAlign(CENTER);
    text('Great that you agree, because your data has already been sold :)', windowWidth/2, windowHeight/2);
  }

//this is the function for the "It is false" button
  function tolate(){
    clear();
    button.hide();
    button2.hide();
    button3.hide();
    button4.hide();
    background('black');
    fill('white');
    textSize(30);
    textAlign(CENTER);
    text('It is too late. Your data has already been sold :)', windowWidth/2, windowHeight/2);
  }

  function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
  }

