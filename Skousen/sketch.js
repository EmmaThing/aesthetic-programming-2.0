let = vaskimg;
let = video;

function setup (){
  createCanvas(750, 1334);
  //background(51);
  video = createCapture(VIDEO);
  video.size(750, 1334);
  video.hide();
}

function draw () {
  imageMode(CENTER);
  image(video, 400, 400);

  imageMode(CENTER);
  image(vaskimg, 410, 500, 200, 200);
}


function preload() {
  vaskimg = loadImage('vaskemaskine.jpg');
}
