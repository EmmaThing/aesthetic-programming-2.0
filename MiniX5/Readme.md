# Generative Program
[See my project](https://emmathing.gitlab.io/aesthetic-programming-2.0/MiniX5/)

[See my repository](https://gitlab.com/EmmaThing/aesthetic-programming-2.0/-/blob/master/MiniX5/sketch.js)

![](randomness.png)

I found this MiniX extremely challenging and I’m not even sure, if I completed the tax for this week, but I tried… 
I became very frustrated many times in this MiniX, because I had a hard time rapping my head around the idea of randomness. One of the main questions that came to my mind again and again, was, is anything really random? We always hear that nothing is neutral in design and technology, so if that is true, then randomness in a computational context, can’t be compared to randomness in the context that things happen by chance, from my perspective. Everything is designed and there by not random?

My generative program shows 7 cols and 5 rows of ellipses in different colors, made with the _random()_ syntax, which can also be called a _pseudorandom_ generator. Pseudorandom is explained as a computational generator, that appears to be random, but isn’t: “Donald Knuth explains, “isn’t random, but it appears to be” (3)—that is, it is “pseudorandom.””. This is for me a great example of the complexity of this topic. 
In the program appears a yellow rectangle (rect()), a white circle and a black circle (ellipse()) as well. These functions are related to the rules I have created. The first rule is that for every yellow rectangle, either a white or a black ellipse should randomly appear. My second and third rule is related to the way the random colored ellipses move to the next position of the array when it reaches a certain point. 
I used a for-loop to repeat the pattern that the ellipses make. Over time more ellipses appear and after all of the points in the array has an ellipse, it clears and start all over again. 


### References:
Nick Montfort et al., “Randomness”, in Montfort et al, 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, Cambridge, MA: MIT Press, 2012, pp. 119-146.

Soon, Winnie. Cox, Geoff. "A Handbook of Software Studies". 2020.
