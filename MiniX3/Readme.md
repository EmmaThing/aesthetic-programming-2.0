## MiniX3

[See my project](https://emmathing.gitlab.io/aesthetic-programming-2.0/MiniX3/)

[See my repository](https://gitlab.com/EmmaThing/aesthetic-programming-2.0/-/blob/master/MiniX3/sketch.js)

### Screenshots of my throbber:

![](MiniX3-picture2.png)
![](MiniX3-picture.png)

My throbber is an illustration of time, that questions the relationship between digital temporality and the temporality that humans experience. 

Visually my program has a png of the world in the middle, a horizontal line across the screen and an ellipse illustrating the sun, circling around the world, controlling the color of the background. To make the design I used syntaxes as, rotate(radians(rotation)), let rotation = 360/num*(frameCount%num) and an Boolean if-statement; if (rotation >= 120 && rotation <= 270). I also created new function as function drawElements() and function preload(). 
To help me determine the degress of the rotations in the if-statement, I learned to use the console, to print the location of the ellipses.

In my design I want to express the difficulties that can be appear, when we experience things differently, as we see the world through digital technologies. Humans have a time cyclic that is defined by the light and the dark as day and night. I wanted my design to create the illusion of day and night, which gave me the idea of the ellipse, illustrating the sun and the changing of the background, between light blue and "the dark". I created exactly 12 ellipses to symbolize the numbers of the clock, but the program placed the ellipses differently than anticipated, which I found very interesting, because it underlined the gap between human’s intention and the doing of digital technologies. 
In the text from the chapter “how humans and machines negotiate experience of time” by Hans Lammerant stands:

_“technology has been a tool through which humans create distance from natural cycles and design their own time experiences.”_ (pp. 96)

I found this quote very suiting for my idea. I feel that I created distance to the natural cycle of day and night, by not following the _”rules”_ of the exact cycle. I created my own experience of time, but on the other hand, I still appealed to the logic of the human mind. Every person I showed my design immediately understood the idea, but no one questioned the construction of it, it became a reality of my time experience. 

I very much like the design, because I see it as ironic, to create the illusion of a natural time, that is nothing like the _real time_, since it only takes a few seconds to go from day to night in my program. It reminds me of the time in The Sims. The Sims universe reflects the natural cycle of night and day as well, but it is still designed in the digital temporality to make it more efficient and fun for the game.



### Reference:

Lammerant, Hans. (2018). How humans and machines negotiate experience of time, in _The Techno-Galactic Guide to Software Observation. (pp. 88-98)

