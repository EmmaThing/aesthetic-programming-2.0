let img;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(5);
}

function draw() {
  background(30);
  drawElements();

//for the picture
  imageMode(CENTER);
  image(img, windowWidth/2, windowHeight/2)

// the line in the middel across the screen
  strokeWeight(4);
  line(0, windowHeight/2, windowWidth, windowHeight/2);

}

// for the "sun" and the rotation
function drawElements() {
  let num=12;
  let rotation = 360/num*(frameCount%num);

//for the background change
  if (rotation >= 120 && rotation <= 270){
    print('sun')
    fill(204, 204, 255);
    rect(0, 0, windowWidth, windowHeight/2);
    } else {
      print('not')
      fill(204, 204, 255);
      rect(0, windowHeight/2, windowWidth,  windowHeight,);
    }

  push();
  translate(width/2, height/2);
  print(rotation)
  rotate(radians(rotation));
  noStroke();
  fill(255, 255, 0);
  ellipse(35, 200, 22, 22);
  pop();
}

function windowResized(){
resizeCanvas (windowWidth, windowHeight);
}

function preload (){
  img = loadImage('Earth-Free-Download-PNG.png');
}
