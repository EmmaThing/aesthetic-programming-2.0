# MiniX6

[See my project](https://emmathing.gitlab.io/aesthetic-programming-2.0/MiniX6/)

[See my repository](https://gitlab.com/EmmaThing/aesthetic-programming-2.0/-/blob/master/MiniX6/sketch.js)

![](MiniX6.png)

**Which MiniX do you plan to rework?**
I have chosen to rework my MiniX5 about the generative program. I chose this MiniX, because I received a very great feedback from Erna and Eva, that after my discussing with others in this week’s instructor-class, made a huge impact on me and pushed me to reflect on former Aesthetic Program topics in a different way. 
Previously in this semester, we talked about the use of data capture in relation to surveillance capitalism. I found this topic very interesting and relevant. We mostly collect data in the intention of using it, so the digital technologies we design can be developed and specified to our needs and wants. 
The idea of developing and improving everything, that is implemented in our digital culture today, effected the way I read this week’s assignment, without me even noticing. I thought that we had to take one of our former MiniXs and make it better, but when I went back to the description of the assignment after the instructor-class, I saw that it said “rework” and I hit me that it didn’t necessarily have to mean “make it _better_ “, but maybe just “ _change_  something”. 
When I received the feedback from Erna and Eva, I also received data about their experiences and the thoughts they had when they saw my program. I could use this data to make my program “better” or I could create some sort of critic, to the stereotypical way of thinking and make my design “worse”. So that is what I did.  

**What have you changed and why?**
I changed almost every part of my program that Erna and Eva expressed that they liked, sort as the colors and the “blinking” of the ellipses. (I just commented the old syntaxes in my code out, so you can see the difference). I removed the fillColor() variable and replaced it with the simple fill() syntax. Then I only made it possible for the program to choose between different shades of gray instead of the different colors as before. I also changed the maxElle variable from 50 to 5, which made the ellipses appear faster and made them stop “blinking”. 
I added a red stroke to the white ellipse, and I changed the 50/50 function of the random generator in my rule about the white and black ellipse (and the yellow rectangle) to be 2/3-part black.  

**How would you demonstrate aesthetic programming in your work?**
The aesthetic element of my program this time, is as explained earlier more plain now, because I wanted to remove the random generating colors. Erna and Eva used the phrase “aesthetically pleasing”, which I found very ironic with my changes now.
 
 
**What does it mean by programming as a practice, or even as a method for design?**
I think programming as a method for design can be extremely useful, positive, negative and impactful. I believe that programming is just another way to express some sort of work and I think that it today is one of the most effective ways to reach a lot of people. It is all about how and for what you want to use it.

**What is the relation between programming and digital culture?**
In the past few weeks where we have learned to program, it has become clear and clear to me that programming means so much more that just making an ellipse or a color appear in a web browser, it is filled with decisions and it reflects our thoughts and feelings, like art does for many. When learning to program, I now know that it is extremely important to be critical and to have in mind that nothing is neutral, that you by using one form of face-tracker can exclude black women or that you by making something in a certain color or shape like an emoji can appeal uncongenially to something in the user’s mind, that can either be reflected badly or good. For me this question about the relation between programming and digital culture is very interesting and important, because I think that it has so much more power over our interactions with each other as humans than we think and therefore we need to study this aspect and be critical in order to reflect on and understand the world we live in.

**Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics?**
In this read.me, I have talked a lot about how important it is to reflect and to be critical towards the way we design and use digital technologies today, which is exactly the same message that the text, “Critical Making and Interdisciplinary Learning”, wants to send. 


### References:

Aesthetic Programming. "A handbook of Software Studies" Soon, Winnie. Cox, Geoff.

Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28.
