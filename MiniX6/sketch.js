let xDim = 1500; //canvas size-width
let yDim = 1000; //canvas size-height
let timer = 30;
let maxElle = 5; //changed from 50 to 5
let elle=0;
let xPos=[1,2,3,4,5,6,7]; // 7 cols
let yPos=[1,2,3,4,5]; // 5 rows
let xCtr=0;
let yCtr=0;
let itr=0;
let currentMillis;
// let fillColor;
let state = 1; //white = 1, yellow = 2, black = 3
let bpup; //makes the ellipse white or black


function setup(){
  createCanvas(xDim, yDim);
  background(240);

  for(let i = 0; i < xPos.length; i++) {
    xPos[i] = xPos[i] * (xDim / (xPos.length+1));
}

  for(let i = 0; i < yPos.length; i++) {
    yPos[i] = yPos[i] * (yDim / (yPos.length+1)); }
    //fillColor = color(floor(random(0, 255)),floor(random(0, 255)),floor(random(0, 255)));
}

function draw() {
if(currentMillis = timer){

translate(xPos[xCtr], yPos[yCtr]); //rows and cols
//print(xPos);
//print(yPos);

//fill(fillColor);
fill(random(150));
noStroke();
ellipse(40, 40, 120, 40);

elle++; //change color
if(elle >= maxElle){ //reach the max for each asterisk
xCtr++; //move to next array
//meet max cols, and need to go to next row
if(xCtr >= xPos.length) {
xCtr = 0;
yCtr++; //next row

if(yCtr >= yPos.length){
  yCtr = 0;
  background(240);
  }
}

elle = 0;
// fillColor = color(floor(random(0,255)),floor(random(0,255)),floor(random(0,255)));
}
}
choice();
}

//Rule:
function choice(){
 if (state==2){
   bpup = random(3);
   if (bpup < 1){
     p();
   } else {
     pu();
   }
 } else {
     b();

 }
}


// yellow rect
function b(){
  fill(255, 255, 0);
  noStroke();
  rectMode(CENTER);
  rect((xDim / (xPos.length+2)), (yDim / (yPos.length+2)), 20, 20);
  state = 2;
}

// black ellipse
function pu(){
  fill(1, 1, 0);
  noStroke();
  ellipseMode(CENTER);
  ellipse((xDim / (xPos.length+1)), (yDim / (yPos.length+1)), 23.4, 23.4);
  state = 3;
}

//white ellipse
function p(){
  fill(255, 255, 255);
  //noStroke();
  stroke(255, 0, 0);
  strokeWeight(4);
  ellipseMode(CENTER);
  ellipse((xDim / (xPos.length+1)), (yDim / (yPos.length+1)), 20, 20);
  state = 1;
}
