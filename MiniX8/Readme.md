# You’re disgusting!

Work made by Amalie Sofie Due Jensen and Emma Thing

[**Run our program**](https://emmathing.gitlab.io/aesthetic-programming-2.0/MiniX8/)


[See our repository](https://gitlab.com/EmmaThing/aesthetic-programming-2.0/-/blob/master/MiniX8/sketch.js)

[See our JSON-file](https://gitlab.com/EmmaThing/aesthetic-programming-2.0/-/blob/master/MiniX8/DotGothic16-Regular.ttf)


![](ourprogram.png)

 
Our work is called _**“You’re disgusting!”**_. The piece represents how female influencers sometimes are blamed, judged and commented on in a negative way on instagram, because of their appearance and behaviours on social media. Our work highlights how some people are very quick to judge others based on what they see and what they think something or someone represents and how it is very easy to hide behind technology and smartphones. There is an element of duality in our work because on one side it tries to expose how cruel people can be in a social media context and on the other it also emphasizes on the importance and the responsibility Influencers have when creating unattainable beauty standards. 


Our work starts with a black empty canvas but when you press the spacebar a silhouette of a woman is highlighted through green words and sentences that are displayed in different places in the center of the canvas. When you look even closer, you discover that the sentences are mostly all negative comments about the female appearance. We have collected the sentences exclusively on Instagram from female influencers instagram feed. We mostly collected negative comments but we also included some more positive ones like “perfection”, “sexy” and “gorgeous” because we believe these words also have an influence when it comes to creating certain beauty standards in an online environment. These comments might not be perceived as positive for the receiver though, because their effect can varriere. Our idea with this piece was to expose the other and more controversial side of instagram. We think that some people are very quick to judge and blame others, based on their appearance and behaviours on social media. People create a certain image that rarely reflects the true picture and when someone does something wrong people hide behind their screens and write things they maybe wouldn't say face to face. 


We have used the _random_ syntax to make the words from our json-file appear in random places but still focused on the center of the canvas where the silhouette is.(instagram.json). We have also used the loadJSON, loadSound, loadImage and loadFont in a preload function (lines 12-15) to load the picture of the female silhouette, load a typing effect, use a different font and include the data collected from instagram in our program. The main syntax in our program is the text element (lines) which we used to manipulate where the Json data was displayed on the canvas. 
We have also played a little around with the sound effect of typing on a keyboard, which is played in the background, when the silhouette and text appear, to underline that these comments are real and that “someone is typing them”. Both of us used this syntax for the first time.


With our program, we tried to set focus on the power of words, just as the e-lit genre does. In our developing process we talk a lot about how people tend to put others in some sort of box (creating stereotypes), the “need” to “blame” each other for things and that we wanted to set focus on vocal abuse. 
Words have power, as Winnie Soon wants to show with the work; _Vocable Code_. In a digitalised world as today, we communicate with the whole word and almost everyone has the ability to express themselves on social media, this also means that there are a lot of opinions. In Soon’s work, the typical Boolean statements, true and false, which affect a variable’s status, is renamed _notTrue_ and _notFalse_. We found this detail very interesting because it made us reflect upon the use of words and made us realise that we blindly put these statements into our programming culture without questioning the meaning of it. As written in the description of the work; _“notTrue and  notFalse suggest an undoing of binary relations”_ (p. 175) and therefore challenge the idea of something being wrong and right. This perspective is important to understand Soon’s work, but also important to the message we want to send with our design. We need to think about how we express ourselves on social media and how the words we use, affects both our culture and the perspective of the person that receives our comment. The womanly figure is created by words and the illusion of her is a metaphor for word’s ability to form our life and the way we understand it. 
The fact that you aren't able to see the silhouette of the woman before you press the spacebar and the mean and disgusting comments are displayed, has a very powerful effect. The woman is in a way defined and identified by these comments which creates a very unhealthy insight in social media culture.  


It was not that difficult for us to find these hate comments on female influencers instagram pages, even though we believe that a lot of them are filtered out according to instagram policies. Another aspect is that we obviously did not have access to people's personal DM’s on instagram where we believe a lot of hate comments are left also so our work only shows a very little part of a very large, powerful and disturbing culture on social media. Our work kind of exposes something that most people already know exists but it is a culture that is not displayed in a straightforward and obvious way. We think it is important to consider the consequences of our behaviours on social media because it has a lot greater impact on social, cultural and societal norms and standards. 
To draw parallels to Vocable Code, our work uses text as the main medium to create a dynamic and disrupted art piece that exposes a social and cultural aspect of life, that is not always obvious. We have used a font that kind of resembles the structure of what you find in some high level programming languages and used it to display words and sentences that humans have produced in real life on instagram. Therefore our work blends elements known from computer langage and human language to create a dynamic understanding between technology and reality in similar ways Vocable Code does.  

#### References
Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186

Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38.
