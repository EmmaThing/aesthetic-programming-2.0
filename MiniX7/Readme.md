# MiniX7

[Try my game]( https://emmathing.gitlab.io/aesthetic-programming-2.0/MiniX7/)

[See my repository]( https://gitlab.com/EmmaThing/aesthetic-programming-2.0/-/blob/master/MiniX7/sketch.js)

[See my second repository](https://gitlab.com/EmmaThing/aesthetic-programming-2.0/-/blob/master/MiniX7/Corona.js)

![](CoronaGame.png)
![](GameOver.png)


In this week’s MiniX I have created a game where a humanly figure, at the bottom of the screen, has to avoid the falling “coronaviruses”, to not get sick. You move the figure with the left and right arrows on the keyboard. The game is over when you catch 8 “coronaviruses”. I had inspiration from Winnies Tofu Game.

The objects in my program are the falling “coronaviruses”. They have a speed-range from 1 to 10, a size-range from 25 to 60 and a position that is depending on the vector syntax. 

When I started to think about my game, I was thinking back on all the games I played as a child, such as Bubble Struggle and The Sims, these games became my inspiration.
In instructor class, I was talking with my group and Nynne. We discussed among others, how games often are a reflection on our “real” world and used The Sims as an example. I found this discussion very interesting and therefore I wanted to make something, that was a reflection on the situation we live in right now with Covid-19. We are trying to avoid getting sick, but it is hard, because it is everywhere. It has become not a game, but a fight and that is also why I decided that it is not possible to win the game, you can only lose and get “sick”.

### References:

Tofu Game. https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch6_ObjectAbstraction/ Soon, Winnie. 

Aesthetic Programming. "A handbook of Software Studies" Soon, Winnie. Cox, Geoff.
