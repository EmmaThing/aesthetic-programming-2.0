/*
let personSize = {
  w:150,
  h:150
};

let sick=0, health=0;
let coronaarray = [];
let coronaimg;
let min_corona = 15;
let mini_width;
let personimg;
let personPosX;

function preload() {
  coronaimg = loadImage('coronavirus.png');
  personimg = loadImage('person.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  personPosX = width/2;
  mini_width = width;
  //emojiX =
  //emojiY = width/2;
  //emoji(emojiX, emojiY);
}

function draw () {
  background(100);
  displayScore();
  checkCoronaNum();
  showCorona();

  imageMode(CENTER);
  image(personimg, personPosX, windowHeight-50, personSize.w, personSize.h);

  checkCatch();
  checkResult();
}

function checkCoronaNum(){
  if(coronaarray.length < min_corona){
    coronaarray.push(new Corona());
  }
}

function showCorona(){
  for(let i=0; i < coronaarray.length; i++){
    coronaarray[i].move();
    coronaarray[i].show();
  }
}

function checkCatch(){
  for (let i = 0; i < coronaarray.length; i++) {
  let d = int(
    dist(personPosX+personSize.w/4, height-personSize.h/4,
      coronaarray[i].pos.x, coronaarray[i].pos.y)
    );
  if (d < personSize.w/2) {
    sick++;
    coronaarray.splice(i,1);
  } else if (coronaarray[i].pos.y > height) {
    health++;
    coronaarray.splice(i,1);
    }
  }
}

function displayScore() {
    fill(10);
    textSize(17);
    text('You have been exposed to '+ sick + " coronavirus", 10, height/1.4);
    fill(10);
    text('PRESS THE ARROW LEFT & RIGTH KEY TO AVOID GETTING CORONA',
    10, height/1.4+20);
}

function checkResult() {
  if (sick > 7) {
    clear();
    fill(255, 0, 0);
    textSize(50);
    textAlign(CENTER);
    text("You have caught Covid-19... GAME OVER", width/2, height/2);
    noLoop();
  }
}

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    personPosX-=25;
  } else if (keyCode === RIGHT_ARROW) {
    personPosX+=25;
  }

  if (personPosX > mini_width) {
    personPosX = mini_width;
  } else if (personPosX < 0 - personSize.w/2) {
    personPosX = 0;
  }
}
*/

let personSize = {
  w:150,
  h:150
};

let sick=0, health=0;
let corona = [];
let coronaimg;
let min_corona = 15;
let mini_width;
let personimg;
let personPosX;

function preload() {
  coronaimg = loadImage('coronavirus.png');
  personimg = loadImage('person.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  personPosX = width/2;
  mini_width = width;
  //emojiX =
  //emojiY = width/2;
  //emoji(emojiX, emojiY);
}

function draw () {
  background(100);
  displayScore();
  checkCoronaNum();
  showCorona();

  imageMode(CENTER);
  image(personimg, personPosX, windowHeight-50, personSize.w, personSize.h);

  checkCatch();
  checkResult();
}

function checkCoronaNum(){
  if(corona.length < min_corona){
    corona.push(new Corona());
  }
}

function showCorona(){
  for(let i=0; i < corona.length; i++){
    corona[i].move();
    corona[i].show();
  }
}

function checkCatch(){
  for (let i = 0; i < corona.length; i++) {
  let d = int(
    dist(personPosX+personSize.w/4, height-personSize.h/4,
      corona[i].pos.x, corona[i].pos.y)
    );
  if (d < personSize.w/2) {
    sick++;
    corona.splice(i,1);
  } else if (corona[i].pos.y > height) {
    health++;
    corona.splice(i,1);
    }
  }
}

function displayScore() {
    fill(10);
    textSize(17);
    text('You have been exposed to '+ sick + " coronavirus", 10, height/1.4);
    fill(10);
    text('PRESS THE ARROW LEFT & RIGTH KEY TO AVOID GETTING CORONA',
    10, height/1.4+20);
}

function checkResult() {
  if (sick > 7) {
    clear();
    fill(255, 0, 0);
    textSize(50);
    textAlign(CENTER);
    text("You have caught Covid-19... GAME OVER", width/2, height/2);
    noLoop();
  }
}

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    personPosX-=25;
  } else if (keyCode === RIGHT_ARROW) {
    personPosX+=25;
  }

  if (personPosX > mini_width) {
    personPosX = mini_width;
  } else if (personPosX < 0 - personSize.w/2) {
    personPosX = 0;
  }
}

