class Corona{
  constructor(){
    this.speed = floor(random(1, 10));
    this.size = floor(random(35, 60));
    this.pos = new createVector(random(windowWidth), 0);
  }
  move(){
    this.pos.y += this.speed;
  }
  show(){
    imageMode(CENTER);
    image(coronaimg, this.pos.x, this.pos.y, this.size, this.size);
    }
  }
